scalaVersion := "2.9.2"

libraryDependencies ++= Seq(
  "org.jsoup" % "jsoup" % "1.6.3",
  "org.scalatest" %% "scalatest" % "1.8" % "test"
)
