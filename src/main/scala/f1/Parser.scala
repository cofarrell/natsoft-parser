package f1

import org.jsoup.Jsoup

object Parser {

  def getLines(in: java.io.InputStream) = {
    val doc = Jsoup.parse(in, "UTF-8", "/")
    val pre = doc.select("pre").get(2).select("font").text
    pre.split("\n").toList.drop(3).takeWhile(_.length > 0)
  }

  def parse(in: java.io.InputStream) = {
    for (line <- getLines(in)) yield {
      val driver = line.substring(4, 27)
      val times = for (i <- 1 to 10) yield {
        val offset = 28 + (i - 1) * 10
        if (line.length > offset) Some(line.substring(offset, offset + 9)) else None
      }
      (driver.trim, times.flatten.min)
    }
  }

  def guessColumns(in: java.io.InputStream) = {
    val pre = getLines(in).map(_.padTo(150, ' '))
    val rowSpaceIndexes = pre.map(line => line.zipWithIndex.filter(_._1 == ' ').map(_._2).toSet).toList
    val allCommonSpaces = rowSpaceIndexes.reduce((a, b) => a & b).toList.sorted
    // List(1, 3, 4, 6, 7) => List(1, 4, 7)
    val commonSpaces = allCommonSpaces.foldRight(List[Int]()) {
      case (a, Nil) => a :: Nil
      case (a, h :: t) => if (h == a + 1) a :: t else a :: h :: t
    }
    val ranges = (0 :: commonSpaces).zip(commonSpaces)
    pre.map(line => ranges.map { case (s, e) => line.substring(s, e).trim })
  }
}
