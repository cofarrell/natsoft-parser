package f1

import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers

class ParserTest extends FlatSpec with ShouldMatchers {

  def parse(name: String) = Parser.parse(classOf[ParserTest].getResourceAsStream(name + ".html"))
  def guessColumns(name: String) = Parser.guessColumns(classOf[ParserTest].getResourceAsStream(name + ".html"))

  "A Parser" should "parse values" in {
    parse("15072012.QR1.Q1.I") should equal(List(
      ("Phillip Holzberger", "1:28.4023"), ("Gary Craig", "1:28.9632"), ("Dwayne Taylor", "1:30.4431"), ("Murray Kent", "1:32.0450"), ("Callum Whatmore", "1:32.2993"), ("Steven Calverley", "1:33.6378"), ("Tony Saint", "1:34.7121"), ("Todd Waldon", "1:35.1112"), ("Mick Apps", "1:39.6345"), ("Shayne Melton", "1:40.4470"), ("Gavin McAlpin", "1:44.5254")))

    println(guessColumns("15072012.QR1.Q1.I").mkString("\n"))
    println()
    println(guessColumns("15072012.PCR.WH2.I").mkString("\n"))
    println()
    println(guessColumns("29012012.WINO.P1.I").mkString("\n"))
  }
}
